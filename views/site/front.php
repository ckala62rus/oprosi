<?php

echo '<pre>';

var_dump($result);

foreach($result as $position_key => $question_key)
{
    foreach($question_key as $element => $otveti)
    {
        echo $element . ' ' . $otveti . '<br>';

        if ( is_array( $otveti ) )
        {
            foreach($otveti as $key => $value)
            {
                echo $key . ' ' . $value . '<br>';
            }
        }
    }
}

echo '</pre>';

?>
<?php foreach ($otveti as $key => $value): ?>

<?php endforeach; ?>
<!--
<div class="container">
    <div class="row">
        <div class="surveys-question col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
            <h1>Ответьте, пожалуйста, на вопрос</h1>
        </div>

        <div class="surveys-question-step col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
            <div class="surveys-question-step-block">
                <p>Шаг</p>
                <p>4 из 6</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="content">
            <div class="content-question">
                <h2>Как часто вы обедаете в кафе? Как часто вы обедаете в кафе</h2>
            </div>

            <div class="content-answer">

                <div class="row">
                    <div class="checkbox col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                        <input type="checkbox" name="check">
                    </div>

                    <div class="checkbox-answer col-xl-11 col-lg-11 col-md-11 col-sm-11 col-11">
                        <p>Предпочитаю отмечать дома</p>
                    </div>
                </div>

                <div class="row">
                    <div class="checkbox col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                        <input type="checkbox" name="check">
                    </div>

                    <div class="checkbox-answer col-xl-11 col-lg-11 col-md-11 col-sm-11 col-11">
                        <p>В ближайшее время нет праздников</p>
                    </div>
                </div>

                <div class="row">
                    <div class="checkbox col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                        <input type="checkbox" name="check">
                    </div>

                    <div class="checkbox-answer col-xl-11 col-lg-11 col-md-11 col-sm-11 col-11">
                        <p>Планирую, но не у вас</p>
                    </div>
                </div>

                <div class="row">
                    <div class="checkbox col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                        <input type="checkbox" name="check">
                    </div>

                    <div class="checkbox-answer col-xl-11 col-lg-11 col-md-11 col-sm-11 col-11">
                        <p>Хочу получать специальное предложение</p>
                    </div>
                </div>

            </div>

            <div class="block-button">
                <div class="next-button">
                    <a href="#">Продолжить</a>
                </div>
            </div>

            <div class="link-site">
                <div class="link-site-a">
                    <a href="#"><u>klientwifi.ru</u></a>
                </div>
            </div>

        </div>
    </div>
</div>
-->

<?php foreach ($result as $position_key => $question_key): ?>
<div class="container">
    <div class="row">

        <div class="surveys-question col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
            <h1>Ответьте, пожалуйста, на вопрос</h1>
        </div>

        <div class="surveys-question-step col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
            <div class="surveys-question-step-block">
                <p>Шаг</p>
                <p>4 из 6</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="content">

            <div class="content-question">
                <h2>Как часто вы обедаете в кафе? Как часто вы обедаете в кафе</h2>
            </div>

            <div class="content-answer">

    <?php if (is_array($otveti)): ?>

                <div class="row">

                <?php foreach ($otveti as $key => $value): ?>

                    <div class="checkbox col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                        <input type="checkbox" name="check">
                    </div>

                    <div class="checkbox-answer col-xl-11 col-lg-11 col-md-11 col-sm-11 col-11">
                        <p><?= $value; ?></p>
                    </div>

                <?php endforeach; ?>

                </div>

    <?php endif;?>

            </div>

            <div class="block-button">
                <div class="next-button">
                    <a href="#">Продолжить</a>
                </div>
            </div>

            <div class="link-site">
                <div class="link-site-a">
                    <a href="#"><u>klientwifi.ru</u></a>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<?php endforeach; ?> <!-- конец контента-->