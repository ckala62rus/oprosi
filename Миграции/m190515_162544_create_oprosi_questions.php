<?php

use yii\db\Migration;

/**
 * Class m190515_162544_create_questions_surveys
 */
class m190515_162544_create_oprosi_questions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%questions_oprosi}}', [
            'id' => $this->primaryKey(),
            'oprosi_id' => $this->integer(),
            'questions_id' => $this->integer()
        ]);

        /***************************************/

                $this->createIndex(
                    'my_questions_my_questions',
                    'questions_oprosi',
                    'questions_id'
                );

                $this->addForeignKey(
                    'my_questions_my_questions',
                    'questions_oprosi',
                    'questions_id',
                    'questions',
                    'id',
                    'CASCADE'
                );

        /***************************************/

                $this->createIndex(
                    'my_oprosi_id',
                    'questions_oprosi',
                    'oprosi_id'
                );

                $this->addForeignKey(
                    'my_oprosi_id',
                    'questions_oprosi',
                    'oprosi_id',
                    'oprosi',
                    'id',
                    'CASCADE'
                );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190515_162544_create_questions_surveys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190515_162544_create_questions_surveys cannot be reverted.\n";

        return false;
    }
    */
}
