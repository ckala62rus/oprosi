<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%my_user}}`.
 */
class m190516_190647_create_my_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%my_user}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(25),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%my_user}}');
    }
}
