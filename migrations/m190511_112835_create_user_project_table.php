<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_project}}`.
 */
class m190511_112835_create_user_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_project}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'answer_user' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_project}}');
    }
}
