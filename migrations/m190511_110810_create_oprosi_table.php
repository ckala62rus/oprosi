<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%oprosi}}`.
 */
class m190511_110810_create_oprosi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%oprosi}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'content' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%oprosi}}');
    }
}
