<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_oprosi}}`.
 */
class m190511_113412_create_user_oprosi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_oprosi}}', [
            'id'=>$this->primaryKey(),
            'id_user' => $this->integer(),
            'id_oprosi'=>$this->integer()
        ]);

        /***************************************/

        $this->createIndex(
            'user_id_user_id',
            'questions_answers',
            'questions_id'
        );

        $this->addForeignKey(
            'user_id_user_id',
            'user_oprosi',
            'id_user',
            'my_user',
            'id',
            'CASCADE'
        );

        /***************************************/

        $this->createIndex(
            'id_oprosi_oprosi_id',
            'user_oprosi',
            'id_oprosi'
        );

        $this->addForeignKey(
            'id_oprosi_oprosi_id',
            'user_oprosi',
            'id_oprosi',
            'oprosi',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_oprosi}}');
    }
}
