<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%questions_answers}}`.
 */
class m190511_112508_create_questions_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%questions_answers}}', [
            'id' => $this->primaryKey(),
            'questions_id'=>$this->integer(),
            'answers_id'=>$this->integer(),
            'position' => $this->integer()
        ]);

        /***************************************/

        $this->createIndex(
            'id_questions_questions_id',
            'questions_answers',
            'questions_id'
        );

        $this->addForeignKey(
            'id_questions_questions_id',
            'questions_answers',
            'questions_id',
            'questions',
            'id',
            'CASCADE'
        );

        /***************************************/

        $this->createIndex(
            'id_answers_answers_id',
            'questions_answers',
            'answers_id'
        );

        $this->addForeignKey(
            'id_answers_answers_id',
            'questions_answers',
            'answers_id',
            'answers',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%questions_answers}}');
    }
}
