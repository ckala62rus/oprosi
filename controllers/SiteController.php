<?php

namespace app\controllers;

use app\models\Answers;
use app\models\MyUser;
use app\models\Questions;
use app\models\QuestionsSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /*
     * Фронт опроса
     */
    public function actionFront()
    {
        $User = MyUser::find()->where(['id'=>1])->all();
        $User = $User[0]['name'];

        //$Questions = Questions::find()->all();
        //$Questions = ArrayHelper::map($Questions, 'title', 'content');

        //$Answer = Answers::find()->all();
        //$Answer = ArrayHelper::map($Answer, 'id', 'content');

        /*
            SELECT questions.title, answers.content
            FROM questions, answers
            INNER JOIN questions_answers ON questions_answers.answers_id=answers.id AND questions_answers.questions_id=questions.id
            //WHERE questions.id = 1;
            //questions_answers.questions_id=questions.id AND
        */

        $result = [
            1 => [
                'Позиция'=>'1',
                'Вопрос_1'=>[
                    1=>'Ответ_1',
                    2=>'Ответ_2',
                    3=>'Ответ_3',
                    4=>'Ответ_4',
                ],
            ],

            2 => [
                'Позиция'=>'2',
                'Вопрос_2'=>[
                    1=>'Ответ_2',
                    2=>'Ответ_3',
                    3=>'Ответ_4',
                ],
            ],

        ];

        if (Yii::$app->request->isPost)
        {
            var_dump(Yii::$app->request->isPost);
        }

        return $this->render('front', [
            //'Questions'=>$Questions,
            //'Answer'=>$Answer,
            'result' => $result,
            'User'=>$User,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
