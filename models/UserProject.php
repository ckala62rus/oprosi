<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_project".
 *
 * @property int $id
 * @property string $name
 * @property string $answer_user
 */
class UserProject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['answer_user'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'answer_user' => 'Answer User',
        ];
    }
}
