<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oprosi".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 *
 * @property OprosiQuestions[] $questionsOprosis
 */
class Oprosi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oprosi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsOprosis()
    {
        return $this->hasMany(OprosiQuestions::className(), ['oprosi_id' => 'id']);
    }
}
