<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_oprosi".
 *
 * @property int $id_user
 * @property int $id_oprosi
 * @property string $answers_user
 */
class UserOprosi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_oprosi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_oprosi'], 'integer'],
            [['id_user'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'oprosi_id' => 'Oprosi ID',
            'answers_user' => 'Answers User',
        ];
    }
}
