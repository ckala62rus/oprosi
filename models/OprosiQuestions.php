<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "questions_oprosi".
 *
 * @property int $id
 * @property int $questions_id
 * @property int $oprosi_id
 *
 * @property Oprosi $oprosi
 * @property Questions $questions
 */
class OprosiQuestions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions_oprosi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['questions_id', 'oprosi_id'], 'integer'],
            [['oprosi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Oprosi::className(), 'targetAttribute' => ['oprosi_id' => 'id']],
            [['questions_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['questions_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questions_id' => 'Questions ID',
            'oprosi_id' => 'Oprosi ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOprosi()
    {
        return $this->hasOne(Oprosi::className(), ['id' => 'oprosi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasOne(Questions::className(), ['id' => 'questions_id']);
    }
}
