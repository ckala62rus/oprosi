<?php

namespace app\modules\oprosi\controllers;

use app\models\OprosiSearch;
use app\models\UserOprosi;
use Yii;
use app\models\Myuser;
use app\models\MyuserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MyuserController implements the CRUD actions for Myuser model.
 */
class MyuserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Myuser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MyuserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Myuser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Myuser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Myuser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Myuser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $searchModelOprosi = new OprosiSearch();
        $dataProviderBlock = $searchModelOprosi->search(Yii::$app->request->queryParams);
        $cheked_block = UserOprosi::find()->select('id_oprosi')
            ->where(['id_user'=>$id])->asArray()->all();

        $cheked_block = ArrayHelper::getColumn($cheked_block, 'id_oprosi');

        return $this->render('update', [
            'model' => $model,
            'cheked_block' => $cheked_block,
            'searchModelAnswer' => $searchModelOprosi,
            'dataProviderBlock' => $dataProviderBlock,
        ]);
    }

    /**
     * Deletes an existing Myuser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Myuser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Myuser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Myuser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*
     * Меняем список выбранных ответов для вопроса
     * */
    public function actionAddBlock()
    {

        if (Yii::$app->request->isAjax){

            $request = Yii::$app->request;

            $keys = $request->post('keys');
            $id = $request->post('id');

            UserOprosi::deleteAll(['id_user' => $id]);

            if ($keys != null){

                foreach ($keys as $item){
                    $model = new UserOprosi();
                    $model->id_user = $id;
                    $model->id_oprosi = $item;
                    if (!$model->save()){
                        return $model->errors;
                    };
                }
            }

            return true;

        }
    }
}
