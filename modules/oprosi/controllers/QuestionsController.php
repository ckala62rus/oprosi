<?php

namespace app\modules\oprosi\controllers;

use app\models\AnswersSearch;
use app\models\QuestionsAnswers;
use Yii;
use app\models\Questions;
use app\models\QuestionsSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Questions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $searchModelAnswer = new AnswersSearch();
        $dataProviderBlock = $searchModelAnswer->search(Yii::$app->request->queryParams);
        $cheked_block = QuestionsAnswers::find()->select('answers_id')
            ->where(['questions_id'=>$id])->asArray()->all();

        $cheked_block = ArrayHelper::getColumn($cheked_block, 'answers_id');

        return $this->render('update', [
            'model' => $model,
            'cheked_block' => $cheked_block,
            'searchModelAnswer' => $searchModelAnswer,
            'dataProviderBlock' => $dataProviderBlock,
        ]);
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*
     * Меняем список выбранных ответов для вопроса
     * */
    public function actionAddBlock()
    {

        if (Yii::$app->request->isAjax){

            $request = Yii::$app->request;

            $keys = $request->post('keys');
            $id = $request->post('id');

            QuestionsAnswers::deleteAll(['questions_id' => $id]);

            if ($keys != null){

                foreach ($keys as $item){
                    $model = new QuestionsAnswers();
                    $model->questions_id = $id;
                    $model->answers_id = $item;
                    if (!$model->save()){
                        return $model->errors;
                    };
                }
            }

            return true;

        }
    }

    /**
     * Экшены для виджета Vovan
     */
    public function actions()
    {

        return [
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => Url::home(true).'/upload/images/', // для загрузки в виджет
                'path' => \Yii::getAlias('@webroot').'/upload/images/', // загрузка изображения на диск

            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::home(true).'/upload/images/', // для загрузки в виджет
                'path' => \Yii::getAlias('@webroot').'/upload/images/', // загрузка изображения на диск
                'unique' => false, //не переименовывать файлы
                'replace' => true,
            ],
            'file-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::home(true).'/upload/images/', // для загрузки в виджет
                'path' => \Yii::getAlias('@webroot').'/upload/images/', // загрузка изображения на диск
            ],
        ];


    }
}
