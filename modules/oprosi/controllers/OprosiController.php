<?php

namespace app\modules\oprosi\controllers;

use app\models\OprosiQuestions;
use app\models\QuestionsSearch;
use Yii;
use app\models\Oprosi;
use app\models\OprosiSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OprosiController implements the CRUD actions for Oprosi model.
 */
class OprosiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Oprosi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OprosiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Oprosi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Oprosi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Oprosi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Oprosi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $searchModelAnswer = new QuestionsSearch();
        $dataProviderBlock = $searchModelAnswer->search(Yii::$app->request->queryParams);
        $cheked_block = OprosiQuestions::find()->select('questions_id')
            ->where(['oprosi_id'=>$id])->asArray()->all();

        $cheked_block = ArrayHelper::getColumn($cheked_block, 'questions_id');

        return $this->render('update', [
            'model' => $model,
            'cheked_block' => $cheked_block,
            'searchModelAnswer' => $searchModelAnswer,
            'dataProviderBlock' => $dataProviderBlock,
        ]);
    }

    /**
     * Deletes an existing Oprosi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Oprosi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Oprosi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Oprosi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*
     * Меняем список выбранных ответов для вопроса
     * */
    public function actionAddBlock()
    {

        if (Yii::$app->request->isAjax){

            $request = Yii::$app->request;

            $keys = $request->post('keys');
            $id = $request->post('id');

            OprosiQuestions::deleteAll(['oprosi_id' => $id]);

            if ($keys != null){

                foreach ($keys as $item){

                    $model = new OprosiQuestions();
                    $model->oprosi_id = $id;
                    $model->questions_id = intval($item);
                    if (!$model->save()){
                        return json_encode($model->errors);
                    };
                }
            }

            return true;

        }
    }
}
