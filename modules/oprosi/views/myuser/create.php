<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Myuser */

$this->title = 'Create Myuser';
$this->params['breadcrumbs'][] = ['label' => 'Myusers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="myuser-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
