<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Oprosi */

$this->title = 'Update Oprosi: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Oprosis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="oprosi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?= \yii\grid\GridView::widget([
        'id' => 'block_grid',
        'dataProvider' => $dataProviderBlock,
        'filterModel' => $searchModelBlock,
        'columns' => [

            [
                'attribute'=>'id',
                'label' => 'ID',
                'filter' => false,
            ],
            [
                'attribute'=>'content',
                'label' => 'Блоки',
                'filter' => true,
            ],

            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'ReqRoutForm[req_ids][]',
                'checkboxOptions' => function ($model, $key, $index, $column) use ($cheked_block) {
                    $checked = in_array($key,$cheked_block);
                    return ['form'=>'req-rout-form','value' => $key, 'checked'=>$checked];
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                'template' => '{update}',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            \yii\helpers\Url::to(['/oprosi/questions/update', 'id' => $model->id]),
                            ['title' => 'Подробнее','target' => '_blank']);
                    }
                ],
            ],

        ],
    ]); ?>
    <?= Html::button('Сохранить набор', ['id' => 'btn_block_list', 'class' => 'btn btn-success']) ?>

</div>

<?php

$js = <<<JS

$(document).ready(function() {
    
    //добавление блоков в дело
    $("#btn_block_list").on("click", function(e){

        e.preventDefault();
        var keys = $("#block_grid").yiiGridView("getSelectedRows");
        var id = $model->id;

        $.ajax({
            url: "/oprosi/oprosi/add-block",
            type: "POST",
            data: {keys: keys,id:id},
            success: function(data){
                alert("Данные успешно сохранены");
                //console.log(data);
            }
        })

    });

});
JS;
$this->registerJs($js);
?>