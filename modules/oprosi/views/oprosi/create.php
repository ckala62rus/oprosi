<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Oprosi */

$this->title = 'Create Oprosi';
$this->params['breadcrumbs'][] = ['label' => 'Oprosis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oprosi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
